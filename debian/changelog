python-pyorick (1.5-1) unstable; urgency=medium

  * New upstream release
    + remove patch collections_abc
  * Bug fix: "FTBFS: ImportError: Failed to import test module: pyorick",
    thanks to Santiago Vila (Closes: #1095374).
    + new patch complex128

 -- Thibaut Paumard <thibaut@debian.org>  Fri, 14 Feb 2025 11:27:20 +0100

python-pyorick (1.4-4) unstable; urgency=medium

  * Bug fix: "FTBFS: dh_auto_test: error: pybuild --test -i
    python{version} -p &quot;3.10 3.9&quot; returned exit code 13", thanks
    to Lucas Nussbaum (Closes: #1002212).
  * Bump standards version and debhelper compatibility level. No changes
    required.

 -- Thibaut Paumard <thibaut@debian.org>  Sun, 23 Jan 2022 14:13:00 +0100

python-pyorick (1.4-3) unstable; urgency=medium

  * Team upload.
  * Drop Python 2 support.
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * Use debhelper-compat instead of debian/compat.
  * d/watch: Use https protocol.

 -- Ondřej Nový <onovy@debian.org>  Tue, 27 Aug 2019 11:22:23 +0200

python-pyorick (1.4-2) unstable; urgency=low

  * Bug fix: "pyorick fails to import", thanks to Helmut Grohne (Closes:
    #896329, #896237).
  * Check against policy 4.1.4 (change priority from extra to optional,
    use https for Format field of copyright file).
  * Update VCS* fields to salsa.

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 09 May 2018 13:59:37 +0200

python-pyorick (1.4-1) unstable; urgency=low

  * Initial revision (Closes: 785604)

 -- Thibaut Paumard <thibaut@debian.org>  Tue, 19 May 2015 14:22:22 +0200

