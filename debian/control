Source: python-pyorick
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Thibaut Paumard <thibaut@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python,
	       python3-all, python3-setuptools,
	       python3-numpy, yorick (>=2.2.04+dfsg-2~)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/python-pyorick
Vcs-Git: https://salsa.debian.org/science-team/python-pyorick.git
Homepage: https://github.com/LLNL/pyorick/

Package: python3-pyorick
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, yorick (>=2.2.04+dfsg-2~),
	 python3-numpy
Description: Python 3 module to execute Yorick code
 The pyorick package starts Yorick as a subprocess and provides an
 interface between Python and Yorick interpreted code.
 .
 Features:
  + exec or eval arbitrary Yorick code strings
  + get or set Yorick variables
  + call Yorick functions or subroutines with Python arguments
  + get or set slices of large Yorick arrays
  + terminal mode to interact with Yorick by keyboard through Python
 .
 Most of the data is exchanged via binary pipes between the two
 interpreters. Yorick runs in a request-reply mode. Python prints
 anything Yorick sends to stdout or stderr except prompts.
 .
 This package contains the module for Python 3.
